#include <stream9/signals/connection.hpp>

#include <stream9/signals/signal_base.hpp>
#include <stream9/signals/connection_block.hpp>

namespace stream9::signals {

static_assert(std::movable<connection>);

connection::
connection(std::weak_ptr<signal_base> sig, id_type id) noexcept
    : m_signal { std::move(sig) }
    , m_id { id }
{}

connection::
~connection() noexcept
{
    disconnect();
}

bool connection::
is_connected() const noexcept
{
    return !!m_signal.lock();
}

bool connection::
is_blocked() const noexcept
{
    if (auto const sig = m_signal.lock(); sig) {
        return sig->is_blocked(*this);
    }
    else {
        return false;
    }
}

void connection::
disconnect() noexcept
{
    if (auto const sig = m_signal.lock(); sig) {
        sig->disconnect(*this);
        m_signal = {};
    }
}

void connection::
set_block(bool block/*= true*/) noexcept
{
    if (auto const sig = m_signal.lock(); sig) {
        sig->set_block(*this, block);
    }
}

connection_block connection::
block() noexcept
{
    return { m_signal, *this };
}

} // namespace stream9::signals
