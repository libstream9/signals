#include <stream9/signals/connection_block.hpp>

#include <stream9/signals/connection.hpp>
#include <stream9/signals/signal_base.hpp>

namespace stream9::signals {

connection_block::
connection_block(std::weak_ptr<signal_base> sig, connection& con) noexcept
    : m_signal { sig }
    , m_conn { &con }
{
    if (auto const sig = m_signal.lock(); sig) {
        sig->set_block(*m_conn, true);
    }
}

connection_block::
~connection_block() noexcept
{
    if (auto const sig = m_signal.lock(); sig) {
        sig->set_block(*m_conn, false);
    }
}

} // namespace stream9::signals
