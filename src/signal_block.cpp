#include <stream9/signals/signal_block.hpp>

#include <stream9/signals/signal_base.hpp>

namespace stream9::signals {

signal_block::
signal_block(signal_base& sig) noexcept
    : m_signal { &sig }
{
    m_signal->set_block(true);
}

signal_block::
~signal_block() noexcept
{
    if (m_signal) {
        m_signal->set_block(false);
    }
}

} // namespace stream9::signals
