#ifndef STREAM9_SIGNAL_HPP
#define STREAM9_SIGNAL_HPP

#include "signals/signal.hpp"
#include "signals/connection.hpp"
#include "signals/signal_block.hpp"
#include "signals/connection_block.hpp"

namespace stream9 {

namespace sig = signals;

using signals::signal;

} // namespace stream9

#endif // STREAM9_SIGNAL_HPP
