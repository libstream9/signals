#ifndef STREAM9_SIGNALS_CONNECTION_HPP
#define STREAM9_SIGNALS_CONNECTION_HPP

#include <cstdint>
#include <memory>

namespace stream9::signals {

class signal_base;
class connection_block;

/*
 * @model std::movable
 */
class connection
{
public:
    using id_type = std::uintptr_t;

public:
    // essential
    connection(std::weak_ptr<signal_base>, id_type) noexcept;

    ~connection() noexcept;

    connection(connection const&) = delete;
    connection& operator=(connection const&) = delete;

    connection(connection&&) noexcept = default;
    connection& operator=(connection&&) noexcept = default;

    // accessor
    id_type id() const noexcept { return m_id; }

    // query
    bool is_connected() const noexcept;
    bool is_blocked() const noexcept;

    // modifier
    void disconnect() noexcept;

    void set_block(bool = true) noexcept;
    connection_block block() noexcept;

private:
    std::weak_ptr<signal_base> m_signal {};
    id_type m_id {};
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_CONNECTION_HPP
