#ifndef STREAM9_SIGNALS_CONNECTION_BLOCK_HPP
#define STREAM9_SIGNALS_CONNECTION_BLOCK_HPP

#include <memory>

namespace stream9::signals {

class connection;
class signal_base;

/*
 * @model std::movable
 */
class connection_block
{
public:
    // essential
    connection_block(std::weak_ptr<signal_base>, connection&) noexcept;

    ~connection_block() noexcept;

    connection_block(connection_block const&) = delete;
    connection_block& operator=(connection_block const&) = delete;

    connection_block(connection_block&&) = default;
    connection_block& operator=(connection_block&&) = default;

private:
    std::weak_ptr<signal_base> m_signal {};
    connection* m_conn {};
};

} // namespace stream9::signals {

#endif // STREAM9_SIGNALS_CONNECTION_BLOCK_HPP
