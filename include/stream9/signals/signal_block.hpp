#ifndef STREAM9_SIGNALS_SIGNAL_BLOCK_HPP
#define STREAM9_SIGNALS_SIGNAL_BLOCK_HPP

namespace stream9::signals {

class signal_base;

/*
 * @model std::movable
 */
class signal_block
{
public:
    // essential
    signal_block(signal_base&) noexcept;

    ~signal_block() noexcept;

    signal_block(signal_block const&) = delete;
    signal_block& operator=(signal_block const&) = delete;

    signal_block(signal_block&&) = default;
    signal_block& operator=(signal_block&&) = default;

private:
    signal_base* m_signal {};
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_SIGNAL_BLOCK_HPP
