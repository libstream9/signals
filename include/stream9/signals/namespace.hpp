#ifndef STREAM9_SIGNALS_NAMESPACE_HPP
#define STREAM9_SIGNALS_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}

namespace stream9::container {}
namespace stream9::ranges {}
namespace stream9::iterators {}

namespace stream9::signals {

namespace con { using namespace stream9::container; }

namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }

namespace rng::views { using namespace std::ranges::views; }

namespace iter { using namespace stream9::iterators; }

} // namespace stream9::signals {

#endif // STREAM9_SIGNALS_NAMESPACE_HPP
