#ifndef STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_IPP
#define STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_IPP

#include "signal_impl.hpp"
#include "capture_args.hpp"

#include "../connection.hpp"
#include "../namespace.hpp"

#include <concepts>
#include <functional>
#include <memory>
#include <ranges>
#include <utility>

#include <stream9/erase_if.hpp>
#include <stream9/errors.hpp>
#include <stream9/log.hpp>

namespace stream9::signals {

inline auto*
find_receiver(auto& receivers, connection const& c) noexcept
{
    auto it = rng::find_if(receivers,
        [&](auto&& r) {
            return r.id() == c.id();
        });

    return it != receivers.end() ? &*it : nullptr;
}

template<typename F>
auto&
append_receiver(auto& receivers, F&& f)
{
    try {
        receivers.insert({
            .callback = std::forward<F>(f),
        });

        return receivers.back();
    }
    catch (...) {
        rethrow_error();
    }
}

inline void
erase_receiver(auto& receivers, connection const& c) noexcept
{
    erase_if(receivers,
        [&](auto&& r) {
            return r.id() == c.id();
        });
}

template<typename R, typename... Args>
bool signal_impl<R(Args...)>::
is_blocked() const noexcept
{
    return m_block;
}

template<typename R, typename... Args>
bool signal_impl<R(Args...)>::
is_blocked(connection const& c) const noexcept
{
    auto* const r = find_receiver(m_receivers, c);
    if (r) {
        return r->block;
    }
    else {
        return false;
    }
}

template<typename R, typename... Args>
auto signal_impl<R(Args...)>::
active_receivers() const noexcept
{
    return rng::filter_view(m_receivers,
        [sig_block = is_blocked()](auto&& r) {
            return !sig_block && !r.block;
        }
    );
}

template<typename R, typename... Args>
template<typename F>
connection signal_impl<R(Args...)>::
connect(F&& f)
{
    try {
        auto& r = append_receiver(m_receivers, std::forward<F>(f));

        return {
            std::static_pointer_cast<signal_base>(this->shared_from_this()),
            r.id(),
        };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename R, typename... Args>
void signal_impl<R(Args...)>::
set_block(bool const block/*= true*/) noexcept
{
    m_block = block;
}

template<typename R, typename... Args>
void signal_impl<R(Args...)>::
set_block(connection const& c, bool const block/*= true*/) noexcept
{
    auto* const r = find_receiver(m_receivers, c);
    if (r) {
        r->block = block;
    }
}

template<typename R, typename... Args>
template<typename... Args2>
void signal_impl<R(Args...)>::
emit(Args2&&... args) const noexcept
    requires std::same_as<R, void>
{
    if (is_blocked()) return;

    for (auto const& r: active_receivers()) {
        try {
            r.callback(std::forward<Args2>(args)...);
        }
        catch (...) {
            print_error(log::err());
        }
    }
}

template<typename R, typename... Args>
template<typename... Args2>
auto signal_impl<R(Args...)>::
emit(Args2&&... args) const noexcept
    requires (!std::same_as<R, void>)
{
    auto a = capture_args(std::forward<Args2>(args)...);

    return rng::transform_view(active_receivers(),
             [a = std::move(a)](auto& receiver) {
                 try {
                     return std::apply(receiver.callback, a);
                 }
                 catch (...) {
                     return R();
                 }
             });
}

template<typename R, typename... Args>
void signal_impl<R(Args...)>::
disconnect(connection const& c) noexcept
{
    erase_receiver(m_receivers, c);
}

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_IPP
