#ifndef STREAM9_SIGNALS_IMPL_RECEIVER_HPP
#define STREAM9_SIGNALS_IMPL_RECEIVER_HPP

#include <functional>

namespace stream9::signals {

template<typename> struct receiver;

template<typename R, typename... Args>
struct receiver<R(Args...)>
{
    std::function<R(Args...)> callback;
    bool block = false;

    std::uintptr_t id() const noexcept
    {
        return reinterpret_cast<std::uintptr_t>(this);
    }
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_IMPL_RECEIVER_HPP
