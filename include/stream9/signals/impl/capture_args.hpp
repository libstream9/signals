#ifndef STREAM9_SIGNALS_IMPL_CAPTURE_ARGS_HPP
#define STREAM9_SIGNALS_IMPL_CAPTURE_ARGS_HPP

#include <functional>
#include <tuple>
#include <utility>

namespace stream9::signals {

template<typename Arg>
Arg
capture_arg(Arg&& a)
{
    return std::move(a);
}

template<typename Arg>
std::reference_wrapper<Arg>
capture_arg(Arg& a)
{
    return a;
}

template<typename... Args>
auto
capture_args(Args... args)
{
    return std::make_tuple(capture_arg(std::forward<Args>(args))...);
}

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_IMPL_CAPTURE_ARGS_HPP
