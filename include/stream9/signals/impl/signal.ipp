#ifndef STREAM9_SIGNALS_IMPL_SIGNAL_IPP
#define STREAM9_SIGNALS_IMPL_SIGNAL_IPP

#include "signal_impl.hpp"

#include "../signal.hpp"
#include "../signal_block.hpp"

#include <stream9/errors.hpp>

namespace stream9::signals {

template<typename R, typename... Args>
signal<R(Args...)>::
signal()
    try : m_impl { std::make_shared<signal_impl<R(Args...)>>() }
{}
catch (...) {
    rethrow_error();
}

template<typename R, typename... Args>
bool signal<R(Args...)>::
is_blocked() const noexcept
{
    return m_impl->is_blocked();;
}

template<typename R, typename... Args>
template<typename F>
    requires std::is_invocable_r_v<R, F, Args...>
connection signal<R(Args...)>::
connect(F&& f)
{
    return m_impl->connect(std::forward<F>(f));
}

template<typename R, typename... Args>
void signal<R(Args...)>::
set_block(bool const block/*= true*/) noexcept
{
    m_impl->set_block(block);
}

template<typename R, typename... Args>
template<typename... Args2>
    requires std::same_as<R, void>
          && std::invocable<R(Args...), Args2...>
void signal<R(Args...)>::
emit(Args2&&... args) const noexcept
{
    m_impl->emit(std::forward<Args2>(args)...);
}

template<typename R, typename... Args>
template<typename... Args2>
    requires (!std::same_as<R, void>)
          && std::invocable<R(Args...), Args2...>
auto signal<R(Args...)>::
emit(Args2&&... args) const noexcept
{
    return m_impl->emit(std::forward<Args2>(args)...);
}

template<typename R, typename... Args>
signal_block signal<R(Args...)>::
block() noexcept
{
    return { *m_impl.get() };
}

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_IMPL_SIGNAL_IPP
