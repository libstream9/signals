#ifndef STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_HPP
#define STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_HPP

#include "receiver.hpp"

#include "../connection.hpp"
#include "../signal_base.hpp"

#include <stream9/node_array.hpp>

#include <concepts>

namespace stream9::signals {

template<typename> class signal_impl;

template<typename R, typename... Args>
class signal_impl<R(Args...)> : public signal_base
{
public:
    using receiver_list = node_array<receiver<R(Args...)>>;

public:
    // essential
    signal_impl() = default;

    // query
    bool is_blocked() const noexcept;
    bool is_blocked(connection const&) const noexcept override;

    auto active_receivers() const noexcept;

    // modifier
    template<typename F>
    connection connect(F&&);

    void set_block(bool = true) noexcept override;
    void set_block(connection const&, bool = true) noexcept override;

    // command
    template<typename... Args2>
    void emit(Args2&&...) const noexcept
        requires std::same_as<R, void>;

    template<typename... Args2>
    auto // bidirectional_range of value_type R
        emit(Args2&&...) const noexcept
            requires (!std::same_as<R, void>);

private:
    void disconnect(connection const&) noexcept override;

private:
    receiver_list m_receivers;
    bool m_block = false;
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_IMPL_SIGNAL_IMPL_HPP

#include "signal_impl.ipp"
