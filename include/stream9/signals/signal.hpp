#ifndef STREAM9_SIGNALS_SIGNAL_HPP
#define STREAM9_SIGNALS_SIGNAL_HPP

#include <concepts>
#include <memory>
#include <type_traits>

namespace stream9::signals {

class connection;
class signal_block;

template<typename> class signal_impl;

template<typename> class signal;

/*
 * @model std::semiregular
 */
template<typename R, typename... Args>
class signal<R(Args...)>
{
public:
    // essential
    signal();

    // query
    bool is_blocked() const noexcept;

    // modifier
    template<typename F>
        requires std::is_invocable_r_v<R, F, Args...>
    [[nodiscard]] connection
        connect(F&&);

    void set_block(bool = true) noexcept;

    // command
    template<typename... Args2>
        requires std::same_as<R, void>
              && std::invocable<R(Args...), Args2...>
    void emit(Args2&&...) const noexcept;

    template<typename... Args2>
        requires (!std::same_as<R, void>)
              && std::invocable<R(Args...), Args2...>
    [[nodiscard]] auto // bidirectional_range of value_type R
        emit(Args2&&...) const noexcept;

    signal_block block() noexcept;

private:
    std::shared_ptr<signal_impl<R(Args...)>> m_impl;
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_SIGNAL_HPP

#include "impl/signal.ipp"
