#ifndef STREAM9_SIGNALS_SIGNAL_BASE_HPP
#define STREAM9_SIGNALS_SIGNAL_BASE_HPP

#include <memory>

namespace stream9::signals {

class connection;

class signal_base : public std::enable_shared_from_this<signal_base>
{
public:
    virtual ~signal_base() = default;

    // query
    virtual bool is_blocked(connection const&) const = 0;

    // modifier
    virtual void disconnect(connection const&) = 0;

    virtual void set_block(bool = true) = 0;
    virtual void set_block(connection const&, bool = true) = 0;
};

} // namespace stream9::signals

#endif // STREAM9_SIGNALS_SIGNAL_BASE_HPP
